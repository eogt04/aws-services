import io
import boto3
import json
import os

from botocore.exceptions import ClientError

from .config import SNS
from .config import S3
from .config import Textract


class S3():
    """manage the bucket
    """

    def __init__(self):
        self.___s3_instance = None
        self.___s3_resource = None

    @property
    def s3_instance(self):
        if self.___s3_instance is None:
            self.___s3_instance = boto3.client('s3')
        return self.___s3_instance

    @property
    def s3_resource(self):
        if self.___s3_resource is None:
            self.___s3_resource = boto3.resource('s3')
        return self.___s3_resource

    def upload_document(
        self,
        file_object: object,
        file_name: str,
        bucket: str = S3.bucket,
    ) -> bool:
        """Upload file from bytes to S3
        """
        self.___s3_instance = self.s3_instance
        if file_object is None:
            raise
        try:
            self.___s3_instance.upload_fileobj(
                io.BytesIO(file_object),
                bucket,
                file_name
            )
        except ClientError:
            # ToDo logger, bugsnag
            return False
        return True

    def download_document(
        self,
        path: str,
        bucket: str = S3.bucket,
    ) -> bool:
        """Download a file from S3
        """
        self.___s3_resource = self.s3_resource
        try:
            s3_object = self.___s3_resource.Object(bucket, path)
            s3_response = s3_object.get()
            stream = s3_response['Body'].read()
            return stream
        except ClientError as e:
            # ToDo, use logger, bugsnag
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise

    def delete_document(
        self,
        path: str,
        bucket: str = S3.bucket,
    ) -> bool:
        self.___s3_resource = self.s3_resource
        try:
            s3_object = self.___s3_resource.Object(bucket, path)
            s3_object.delete()
        except ClientError as e:
            # ToDo, use logger, bugsnag catch correct exception
            print(":(")
            if e.response['Error']['Code'] == "404":
                print(":(")
            else:
                raise


class SNS():
    """Manage the sns 
    """

    def __init__(self):
        self.___sns_instance = None

    def sns_instance(self, region_name='us-west-2'):
        if self.___sns_instance is None:
            self.___sns_instance = boto3.client('sns', region_name='us-west-2')
        return self.___sns_instance

    def sns_bus_publish(self, result, topic):
        sns = self.sns_instance()
        data = json.dumps(result)
        try:
            response = sns.publish(
                TopicArn=topic,
                Message=data,
            )
            return response
        except:
            return False


class Textract():
    """Manage textract
    """

    def __init__(self):
        self.___textract_instance = None

    def textract_instance(self):
        if self.___textract_instance is None:
            self.___textract_instance = boto3.client(
                'textract',
                region_name=Textract.region_name_textract
            )
        return self.___textract_instance

    def analyze_document(self, image: bytearray) -> dict:
        """Core of OCR, send image and gets the
        json from aws textract
        """
        client = self.textract_instance()
        response = client.analyze_document(
            Document={
                'Bytes': image
            },
            FeatureTypes=["TABLES", "FORMS"]
        )
        return response
