"""Configuración de este paquete"""
from littlenv import littlenv
from os import environ

littlenv.load()


class S3:
    bucket: str = environ.get('S3_BUCKET')


class Textract:
    region_name_textract: str = environ.get(
        'TEXTRACT_REGION_NAME', 'us-east-1')
