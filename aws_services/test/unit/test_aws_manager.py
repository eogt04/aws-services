import os
import unittest
import tempfile
import datetime
from unittest import TestCase
from unittest.mock import Mock
from ocr.aws_manager import aws_utils
import dotenv
from ...config import INTEGRATION

dotenv.load_dotenv()

class TestAwsTextract(TestCase):

    def setUp(self):
        self.textract = aws_utils.AwsServices().textract
        with open("ocr/aws_manager/test/resources/image.png", "rb") as file:
            self.image = file.read()

    @unittest.skipIf(INTEGRATION, "To activate $ export INTEGRATION_TEST=true")
    def test_analyze_document_response_document(self):
        """test response and correct json
        from aws textract
        """
        json_response = self.textract.analyze_document(self.image)
        self.assertEqual(
            json_response['ResponseMetadata']['HTTPStatusCode'], 200)
        self.assertIsInstance(
            json_response,
            dict
        )
        # ToDo, test correct assertion from aws(tables, (images))


class TestS3Aws(TestCase):

    def setUp(self):
        self.s3 = aws_utils.AwsServices().s_3
        self.tm_file = tempfile.NamedTemporaryFile()
        self.today = str(datetime.datetime.now()).replace(" ","")


    @unittest.skipIf(INTEGRATION, "To activate $ export INTEGRATION_TEST=true")
    def test_upload_and_download_document(self):
        """test upload document in service
        s3
        """
        prhase_to_test = "this is just a test"
        file_name = self.tm_file.name
        with open(file_name, "w") as file:
            file.write(prhase_to_test)

        with open(file_name, "rb") as file:
            tm_file_new = file.read()

        self.s3.upload_document(
            tm_file_new,
            "test-ocr{}".format(self.today)
        )

        bytes_file = self.s3.download_document(
            "test-ocr{}".format(self.today)
        )

        string_bytes_file = bytes_file.decode("utf-8")

        self.assertEqual(
            string_bytes_file,
            prhase_to_test,
            'wrong file'
        )

    def tearDown(self):
        self.tm_file.close()
        self.s3.delete_document(
            "test-ocr{}".format(self.today)
        )


class TestSNS(TestCase):
    #ToDo use moto or something to test aws sns
    pass