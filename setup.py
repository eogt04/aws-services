import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="zinob-aws-services",
    version="0.0.1",
    author="Edwarth Garcia",
    author_email="eogt04@gmail.com",
    description="Module to handle with several aws services",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/eogt04/aws-services",
    packages=['aws_services'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'boto3',
        'littlenv'
    ],
)
